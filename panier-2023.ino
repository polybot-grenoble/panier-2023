#include <Servo.h>

/**
 * Utile lors des tests
*/
#define DEBUG 1

//Pin driver moteur
#define IN1 9
#define IN2 10

//Pin commande Servo
//Le permier servo et le servo de la pente (du bouton)
#define PINSERVO1 5
//Servo proche de la trap <- (^^)
#define PINSERVO2 3

//Capteur distance
#define PINCAP 8

#define OPEN 180
#define CLOSE 0

Servo servoBouton;
Servo servoTrap;

/**
 *  0 strategie qui démare à la presence de kobe
 *  1 strategie qui attend le départ de kobe
**/
int choixStrat = 1;

void setup() {
  // put your setup code here, to run once:
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);

  // attachInterrupt(digitalPinToInterrupt(PINCAP),actionPanier,RISING);

  servoBouton.attach(PINSERVO1);
  servoTrap.attach(PINSERVO2);
  servoBouton.write(OPEN);
  servoTrap.write(OPEN);
}

void loop() {

  //test
  actionPanier();
  
  servoBouton.write(CLOSE);
  servoTrap.write(CLOSE);
  //

}

/**
 * Fonction qui execute la stratégie du panier voulu
 * @param void
 * 
*/
void actionPanier(){

  if(choixStrat){
    /**
     * 
    */
    // attente();
    //Démarage du match
    // unsigned long fin = micros() + 1E6 *130 ;
    // unsigned long temps = 0;
    
    // while(temps < fin){
      ballet(1);
      // unsigned long temps = micros();
    // }
  } else {
    ballet(5);
  }

}

/**
 * Fonction qui attend le départ du robot pour commmencer a activer les actionneurs
*/
void attente(){
  int actif = digitalRead(PINCAP);
  
  servoBouton.write(OPEN);
  servoTrap.write(OPEN);
  
  while(actif){
    actif = digitalRead(PINCAP);
    delay(1000);
  }

}

/**
 * Fonction qui balaye avec les servos
 * le nom...(C'est une vanne)
 * @param nbTourMax : Le nombre de tour du ballet, 1 ouverture et fermeture par servo
*/
void ballet(int nbTourMax){
  int nbTour = 0;
  
  while(nbTour < nbTourMax){

  servoBouton.write(CLOSE);
  servoTrap.write(CLOSE);
  delay(3000);

  changementAngleProgressif(CLOSE,OPEN,servoBouton);
  // servoBouton.write(OPEN);
  delay(3000);

  changementAngleProgressif(OPEN,CLOSE,servoBouton);
  // servoBouton.write(CLOSE);
  delay(1000);

  changementAngleProgressif(CLOSE,OPEN,servoTrap);
  // servoTrap.write(OPEN);
  delay(3000);
  
  changementAngleProgressif(OPEN,CLOSE,servoTrap);

  nbTour++;
  }

  servoBouton.write(CLOSE);
  servoTrap.write(CLOSE);
  return;
}

/**
 * Active le moteur pendant un nb de seconde
 * @param nbSecondes : en seconde
*/
void masseur(int nbSecondes){
  
  digitalWrite(IN1,HIGH);
  digitalWrite(IN2,LOW);
  delay(nbSecondes*1000);
  digitalWrite(IN1,LOW);
  digitalWrite(IN2,LOW);
  
  return;
}

/**
 * Permet de controler la vitesse d'un servo moteur
 * @param angleInitial : Un l'angle actuel du servo
 * @param angleFinal : L'angle d'arrivé du servo
 * @param SV : L'objet servo a manipuler
*/
void changementAngleProgressif(unsigned int angleInitial,unsigned int angleFinal,Servo SV){
  
  if(angleFinal > 180){
    angleFinal = 180;
  }
  else if (angleInitial > 180){
    angleInitial = 180;
  }

  if(angleFinal > angleInitial) {
    for(int i = angleInitial;i < angleFinal; i++ ){
      SV.write(i);
      delay(10);
    }
  } else {
    for(int i = angleInitial;i > angleFinal; i-- ){
      SV.write(i);
      delay(10);
    } 
  }
}
